# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the cinder package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: cinder\n"
"Report-Msgid-Bugs-To: cinder@packages.debian.org\n"
"POT-Creation-Date: 2018-03-05 23:08+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: 8bit\n"

#. Type: string
#. Description
#: ../cinder-common.templates.in:2001
msgid "Cinder volume group:"
msgstr ""

#. Type: string
#. Description
#: ../cinder-common.templates.in:2001
msgid ""
"Please specify the name of the LVM volume group on which Cinder will create "
"partitions."
msgstr ""
